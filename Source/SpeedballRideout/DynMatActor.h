// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/InstancedStaticMeshComponent.h"
#include "DynMatActor.generated.h"

UCLASS()
class SPEEDBALLRIDEOUT_API ADynMatActor : public AActor
{
	GENERATED_BODY()
	
public:	
	ADynMatActor();
	virtual void Tick(float DeltaTime) override;

	// Set fog size
	void setSize(float);
	// Reveal a portion of the fog
	void revealSmoothCircle(const FVector2D& pos, float radius);

protected:
	virtual void BeginPlay() override;
	virtual void PostInitializeComponents() override;

private:
	void UpdateTextureRegions(UTexture2D* Texture, int32 MipIndex, uint32 NumRegions,
		FUpdateTextureRegion2D* Regions, uint32 SrcPitch, uint32 SrcBpp, uint8* SrcData, bool bFreeData);

	// Fog texture size  
	static const int m_textureSize = 512;
	uint8 m_pixelArray[m_textureSize * m_textureSize];
	FUpdateTextureRegion2D m_wholeTextureRegion;
	float m_coverSize;

	UPROPERTY()
	FString Path = TEXT("Material'/Game/Shader/M_Material1.M_Material1'");

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* m_squarePlane;
	UPROPERTY()
		UTexture2D* m_dynamicTexture;
	UPROPERTY()
		UMaterialInterface* m_dynamicMaterial;
	UPROPERTY()
		UMaterialInstanceDynamic* m_dynamicMaterialInstance;
	
};
