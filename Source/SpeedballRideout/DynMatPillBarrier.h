// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/InstancedStaticMeshComponent.h"
#include "DynMatPillBarrier.generated.h"

UCLASS()
class SPEEDBALLRIDEOUT_API ADynMatPillBarrier : public AActor
{
	GENERATED_BODY()

public:
	ADynMatPillBarrier();
	virtual void Tick(float DeltaTime) override;

	// Set size
	void setSize(float);

protected:
	virtual void BeginPlay() override;
	virtual void PostInitializeComponents() override;

private:
	void UpdateTextureRegions(UTexture2D* Texture, int32 MipIndex, uint32 NumRegions,
		FUpdateTextureRegion2D* Regions, uint32 SrcPitch, uint32 SrcBpp, uint8* SrcData, bool bFreeData);

	// Fog texture size  
	static const int m_textureSize = 512;
	uint8 m_pixelArray[m_textureSize * m_textureSize];
	FUpdateTextureRegion2D m_wholeTextureRegion;
	float m_coverSize;

	// Asset paths stored in strings for easy changing
	FString MaterialPath = TEXT("Material'/Game/Shader/M_Custom.M_Custom'");
	FString MeshPath = TEXT("StaticMesh'/Game/PillBarrier.PillBarrier'");

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* mesh;
	UPROPERTY()
	UTexture2D* m_dynamicTexture;
	UPROPERTY()
	UMaterialInterface* m_dynamicMaterial;
	UPROPERTY()
	UMaterialInstanceDynamic* m_dynamicMaterialInstance;

};
