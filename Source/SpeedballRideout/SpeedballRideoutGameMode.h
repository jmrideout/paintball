// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SpeedballRideoutGameMode.generated.h"

UCLASS(minimalapi)
class ASpeedballRideoutGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASpeedballRideoutGameMode();
};



