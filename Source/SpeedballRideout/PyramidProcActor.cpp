// Fill out your copyright notice in the Description page of Project Settings.

#include "PyramidProcActor.h"


// Sets default values
APyramidProcActor::APyramidProcActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	mesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("GeneratedMesh"));
	RootComponent = mesh;
}

// Called when the game starts or when spawned
void APyramidProcActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APyramidProcActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APyramidProcActor::GeneratePyramidMesh()
{
	TArray <FVector> Vertices;
	TArray <FVector> Normals;
	TArray <FProcMeshTangent> Tangents;
	TArray <FVector2D> TextureCoordinates;
	TArray <int32> Triangles;
	TArray <FLinearColor> Colors;
	CreatePyramidMesh(FVector2D(50, 50), Vertices, Triangles, Normals, TextureCoordinates, Tangents, Colors);

	// Create the mesh section, specifying collision.
	mesh->CreateMeshSection_LinearColor(0, Vertices, Triangles, Normals, TextureCoordinates, Colors, Tangents, true);
}

void APyramidProcActor::CreatePyramidMesh(FVector2D HeightWidth, TArray <FVector>& Vertices,
	TArray<int32>& Triangles, TArray<FVector>& Normals, TArray<FVector2D>& UVs,
	TArray<FProcMeshTangent>& Tangents, TArray<FLinearColor>& Colors)
{
	// Set number of vertices
	const int32 NumVerts = 16; // 4 for bottom, 3 for each of the 

	// Store vertices of box for easy reference
	FVector Verts[5];
	Verts[0] = FVector(0, 0, HeightWidth.Y); // Top
	Verts[1] = FVector(-HeightWidth.X / 2, HeightWidth.X / 2, 0); // Front Right
	Verts[2] = FVector(-HeightWidth.X / 2, -HeightWidth.X / 2, 0); // Front Left
	Verts[3] = FVector(HeightWidth.X / 2, HeightWidth.X / 2, 0); // Back Right
	Verts[4] = FVector(HeightWidth.X / 2, -HeightWidth.X / 2, 0); // Back Left

	// Set colors ///////////////////////////
	Colors.Reset();
	Colors.AddUninitialized(NumVerts);
	for (int i = 0; i < NumVerts; i++) { // Colors initiated to grey
		Colors[i] = FLinearColor(0.5f, 0.5f, 0.5f);
	}

	// Reset then initialize arrays /////////
	Triangles.Reset();
	Vertices.Reset();
	Normals.Reset();
	Tangents.Reset();
	// Triangles.AddUninitialized(24); // (4 sides + 2 on bottom) * 3 verts per tri
	Vertices.AddUninitialized(NumVerts);
	Normals.AddUninitialized(NumVerts);
	Tangents.AddUninitialized(NumVerts);

	// Create Sides and triangles ////////////
	// Make Bottom
	{
		Vertices[0] = Verts[1];
		Vertices[1] = Verts[2];
		Vertices[2] = Verts[3];
		Vertices[3] = Verts[4];

		Triangles.Add(0);
		Triangles.Add(1);
		Triangles.Add(2);

		Triangles.Add(1);
		Triangles.Add(3);
		Triangles.Add(2);

		// Set normals facing down
		Normals[0] = Normals[1] = Normals[2] = Normals[3] = FVector(0, 0, -1);
		// Set tangents to X
		Tangents[0] = Tangents[1] = Tangents[2] = Tangents[3] = FProcMeshTangent(1, 0, 0);
	}
	// Make Front Side
	{
		Vertices[4] = Verts[0];
		Vertices[5] = Verts[1];
		Vertices[6] = Verts[2];

		Triangles.Add(4);
		Triangles.Add(6);
		Triangles.Add(5);

		// To simplify normals, I am using 45degrees
		Normals[4] = Normals[5] = Normals[6] = FVector(-1, 0 , 1);
		// Set tangents to Y
		Tangents[4] = Tangents[5] = Tangents[6] = FProcMeshTangent(0, 1, 0);
	}
	// Make Back Side
	{
		Vertices[7] = Verts[0];
		Vertices[8] = Verts[3];
		Vertices[9] = Verts[4];

		Triangles.Add(7);
		Triangles.Add(8);
		Triangles.Add(9);

		// To simplify normals, I am using 45degrees
		Normals[7] = Normals[8] = Normals[9] = FVector(1, 0, 1);
		// Set tangents to Y
		Tangents[7] = Tangents[8] = Tangents[9] = FProcMeshTangent(0, 1, 0);
	}
	// Make Right Side
	{
		Vertices[10] = Verts[0];
		Vertices[11] = Verts[1];
		Vertices[12] = Verts[3];

		Triangles.Add(10);
		Triangles.Add(11);
		Triangles.Add(12);

		// To simplify normals, I am using 45degrees
		Normals[10] = Normals[11] = Normals[12] = FVector(0, 1, 1);
		// Set tangents to X
		Tangents[10] = Tangents[11] = Tangents[12] = FProcMeshTangent(-1, 0, 0);
	}
	// Make Left Side
	{
		Vertices[13] = Verts[0];
		Vertices[14] = Verts[4];
		Vertices[15] = Verts[2];

		Triangles.Add(13);
		Triangles.Add(14);
		Triangles.Add(15);

		// To simplify normals, I am using 45degrees
		Normals[13] = Normals[14] = Normals[15] = FVector(0, -1, 1);
		// Set tangents to X
		Tangents[13] = Tangents[14] = Tangents[15] = FProcMeshTangent(1, 0, 0);
	}


	// Set UVs
	UVs.Reset();
	UVs.AddUninitialized(NumVerts);

	// Flip bottom UV
	UVs[0] = FVector2D(0.f, 0.f);
	UVs[1] = FVector2D(1.f, 0.f);
	UVs[2] = FVector2D(0.f, 1.f);
	UVs[3] = FVector2D(1.f, 1.f);

	UVs[4] = UVs[7] = UVs[10] = UVs[13] = FVector2D(0.f, 0.f);
	UVs[5] = UVs[8] = UVs[11] = UVs[14] = FVector2D(1.f, 0.f);
	UVs[6] = UVs[9] = UVs[12] = UVs[15] = FVector2D(0.5f, 1.0f);
}

void APyramidProcActor::PostActorCreated()
{
	Super::PostActorCreated();
	GeneratePyramidMesh();
}

void APyramidProcActor::PostLoad()
{
	Super::PostLoad();
	GeneratePyramidMesh();
}
