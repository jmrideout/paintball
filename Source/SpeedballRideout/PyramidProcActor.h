// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "PyramidProcActor.generated.h"

UCLASS()
class SPEEDBALLRIDEOUT_API APyramidProcActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APyramidProcActor();
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void GeneratePyramidMesh();
	virtual void CreatePyramidMesh(FVector2D HeightWidth, TArray <FVector>& Vertices,
		TArray<int32>& Triangles, TArray<FVector>& Normals, TArray<FVector2D>& UVs,
		TArray<FProcMeshTangent>& Tangents, TArray<FLinearColor>& Colors);

	virtual void PostActorCreated() override;
	virtual void PostLoad();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY(VisibleAnywhere)
	UProceduralMeshComponent* mesh;
};
