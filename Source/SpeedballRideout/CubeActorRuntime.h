// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RuntimeMeshComponent.h"
#include "CubeActorRuntime.generated.h"

UCLASS()
class SPEEDBALLRIDEOUT_API ACubeActorRuntime : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACubeActorRuntime();
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void GenerateBoxMesh();
	virtual void CreateBoxMesh(FVector BoxRadius, TArray <FVector>& Vertices,
				TArray<int32>& Triangles, TArray<FVector>& Normals, TArray<FVector2D>& UVs,
				TArray<FRuntimeMeshTangent>& Tangents, TArray<FColor>& Colors);

	virtual void PostActorCreated() override;
	virtual void PostLoad();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY(VisibleAnywhere)
	URuntimeMeshComponent* mesh;
	
};
