// Fill out your copyright notice in the Description page of Project Settings.

/*
Jason Rideout
GAM 415

I believe that the 3D object is displayed on screen at BeginPlay. It is just a normal static mesh.
The dynamic material is applied to the static mesh by using "mesh->SetMaterial()".
This occurs in the PostInitializeComponents function.
*/

#include "DynMatPillBarrier.h"
#include "Engine.h"

// Sets default values
ADynMatPillBarrier::ADynMatPillBarrier() : m_wholeTextureRegion(0, 0, 0, 0, m_textureSize, m_textureSize)
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	// Set scale of mesh
	m_coverSize = 1.0f;

	// Create a mesh
	mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PillBarrier"));
	RootComponent = mesh;

	// BlockAll and be a barrier
	mesh->SetCollisionProfileName(UCollisionProfile::BlockAll_ProfileName);
	{
		static ConstructorHelpers::FObjectFinder <UStaticMesh> asset(*MeshPath);
		mesh->SetStaticMesh(asset.Object);
	}

	mesh->TranslucencySortPriority = 100; // Set priority to draw on top of other translucent materials.
	mesh->SetRelativeScale3D(FVector(m_coverSize, m_coverSize, 1));

	// Load the base material from your created material.
	{
		static ConstructorHelpers::FObjectFinder<UMaterial>asset(*MaterialPath);
		m_dynamicMaterial = asset.Object;
	}

	// Create the run-time texture.
	if (!m_dynamicTexture) {
		m_dynamicTexture = UTexture2D::CreateTransient(m_textureSize, m_textureSize, PF_G8);
		m_dynamicTexture->CompressionSettings = TextureCompressionSettings::TC_Grayscale;
		m_dynamicTexture->SRGB = 0;
		m_dynamicTexture->UpdateResource();
		m_dynamicTexture->MipGenSettings = TMGS_NoMipmaps;
	}

	// Initialize array to all black.
	for (int x = 0; x < m_textureSize; ++x)
	{
		for (int y = 0; y < m_textureSize; ++y)
		{
			m_pixelArray[y * m_textureSize + x] = 255;
		}
	}

	// Propagate memory's array to texture.  
	if (m_dynamicTexture)
		UpdateTextureRegions(m_dynamicTexture, 0, 1, &m_wholeTextureRegion, m_textureSize, 1, m_pixelArray, false);
}

// Called when the game starts or when spawned
void ADynMatPillBarrier::BeginPlay()
{
	Super::BeginPlay();

}

void ADynMatPillBarrier::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	// Create a dynamic material instance to swap in the fog texture.  
	if (m_dynamicMaterial)
	{
		m_dynamicMaterialInstance = UMaterialInstanceDynamic::Create(m_dynamicMaterial, this);
		m_dynamicMaterialInstance->SetTextureParameterValue("Glow", m_dynamicTexture);
	}
	// Set the dynamic material to the mesh.  
	if (m_dynamicMaterialInstance)
		mesh->SetMaterial(0, m_dynamicMaterialInstance);
}

void ADynMatPillBarrier::setSize(float s)
{
	m_coverSize = s;
	mesh->SetRelativeScale3D(FVector(m_coverSize, m_coverSize, 1));
}

// Called every frame
void ADynMatPillBarrier::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ADynMatPillBarrier::UpdateTextureRegions(UTexture2D * Texture, int32 MipIndex, uint32 NumRegions, FUpdateTextureRegion2D * Regions, uint32 SrcPitch, uint32 SrcBpp, uint8 * SrcData, bool bFreeData)
{
	// Make sure it exists
	if (Texture->Resource)
	{
		// Texture data storage struct
		struct FUpdateTextureRegionsData
		{
			FTexture2DResource * Texture2DResource;
			int32 MipIndex;
			uint32 NumRegions;
			FUpdateTextureRegion2D * Regions;
			uint32 SrcPitch;
			uint32 SrcBpp;
			uint8 * SrcData;
		};

		FUpdateTextureRegionsData* RegionData = new FUpdateTextureRegionsData;
		RegionData->Texture2DResource = (FTexture2DResource *)Texture->Resource;
		RegionData->MipIndex = MipIndex;
		RegionData->NumRegions = NumRegions;
		RegionData->Regions = Regions;
		RegionData->SrcPitch = SrcPitch;
		RegionData->SrcBpp = SrcBpp;
		RegionData->SrcData = SrcData;

		ENQUEUE_UNIQUE_RENDER_COMMAND_TWOPARAMETER(UpdateTextureRegionsData, FUpdateTextureRegionsData *, RegionData, RegionData, bool, bFreeData, bFreeData, {
			for (uint32 RegionIndex = 0; RegionIndex < RegionData->NumRegions; ++RegionIndex)
			{
				int32 CurrentFirstMip = RegionData->Texture2DResource->GetCurrentFirstMip();
				if (RegionData->MipIndex >= CurrentFirstMip)
				{
					RHIUpdateTexture2D(RegionData->Texture2DResource->GetTexture2DRHI(), RegionData->MipIndex - CurrentFirstMip, RegionData->Regions[RegionIndex], RegionData->SrcPitch, RegionData->SrcData + RegionData->Regions[RegionIndex].SrcY * RegionData->SrcPitch + RegionData->Regions[RegionIndex].SrcX * RegionData->SrcBpp);
				}
			}
			if (bFreeData)
			{
				FMemory::Free(RegionData->Regions);
				FMemory::Free(RegionData->SrcData);
			}
			delete RegionData;
			});
	}

}

